MDDAF.$indicator = {
	showing:false,
	show:function(type,txt,icon,ani,timer,overlay,cb){
		//
		//
		//@param type (string) | indicator type - err,win,info
		//@param txt (string) | indicator text
		//@param

		if(MDDAF.$indicator.timer){
			clearTimeout(MDDAF.$indicator.timer);
		}

		var show = function(type,txt,icon,ani,timer){
			var $indicator = MDDAF._HELPERS.elm("indicator"),
				icon_val = "sti-"+icon+" sti-"+ani,
				overlay = _jQ("#overlay");

			if(overlay){
				overlay.fadeIn();
			}

			$indicator.jq.children("i").attr("class","sti sti-lg "+icon_val);
			$indicator.jq.children(".txt").text(txt||"TEST CODE");
			$indicator.jq.attr("class",type||"info");

			_v($indicator.js,{bottom:"1em"});
			MDDAF.$indicator.showing = true;
			if(typeof(cb) == "function"){
				cb();
			}

			if(_.isNumber(timer)){
				MDDAF.$indicator.timer = setTimeout(MDDAF.$indicator.hide,timer);
			}
		}

		show(type,txt,icon,ani,timer);
	},
	hide:function(cb){
		var $indicator = _jQ("#indicator"),
			$overlay = _jQ("#overlay");

		$overlay.fadeOut();
		_v($indicator,{bottom:"-100%"},{duration:1250});
		this.showing = false;
	},
	init:function(){
		this.hide();
	},
	error:function(msg){
		this.show('err',msg||"THERE HAS BEEN AN ERROR",'close-circle',false,3000);
	},
	success:function(msg){
		this.show('win',msg||"SUCCESS",'check',false,3000);
	},
	msg:function(msg){
		this.show('info',msg||"THIS IS A MESSAGE",'comment',false,5000);
	},
	loading:function(msg,cb){
		if(typeof(msg) == "function" || cb){

		}
		this.show('info',msg||"LOADING...",'config','spin',true,cb);
	}
};
